const jwt = require('jsonwebtoken');
const User = require('../models/user');

async function login(req, res) {

    const query = User.findOne({
        username: req.body.username,
        password: req.body.password
    });

    let user = await query.lean().exec().then( (result) => { return JSON.stringify(result) } );

    if( user == null ){
        res.status(403).send({ error: "Invalid credentials"});    
    }else{
        let token = await new Promise((resolve, reject) => {
        
            jwt.sign(user, 'secretKey', (err, token) => {
                if (err){
                    reject(err);
                } else{
                    resolve(token);
                }
            });
        })
        
        res.status(200).send({ token:token });    
    }
}


function test(req, res) {
    res.status(200).send({
        testResult: req.ingreso
    });
}

function verifyToken(req, res, next) {

    const requestHeader = req.headers['authorization'];

    if (typeof requestHeader !== 'undefined') {
        const token = requestHeader.split(" ")[1];

        jwt.verify(token, 'secretKey', (err, payload) => {

            if (err) {
                res.status(403).send({
                    error: "Token not valid"
                });
            } else {
                req.ingreso = payload;
                next();
            }
        });

    } else {
        res.status(403).send({
            error: "Token missing"
        });
    }

}

module.exports = {login, test, verifyToken};
