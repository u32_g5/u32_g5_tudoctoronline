let Medical = require("../models/medical");


//función guardar lista de Medicos
function saveMedical(req, res){
    
    let myMedical = new Medical(req.body);

    myMedical.save((err, result)=>{
        if(err){
            res.status(500).send({message: err});
        }else{
            res.status(200).send({message:result});
        }
    });
}


// CONSULTA DE MEDICOS
function listMedical (req, res){
    let query = Medical.find({}).sort('content');
    query.exec((err, result)=>{
        if(err){
            res.status(500).send({message:err});
        }else{
            res.status(200).send(result);
        }
    });

}

//consultar medicos por id

function findMedical(req, res){

    let id = req.params.id;
    let query = Medical.findById (id);

    query.exec((err, result)=>{
        if(err){
            res.status(500).send({message: err});
        }else{
            res.status(200).send({message: result});
        }
    });
}

//consultar por especialidad

function especialidadMedical (req, res){

    let search = req.params.search;
    let query;
    if(search){
        query = Medical.find({specialty:search}).sort('created');
    }else{
        query = Medical.find({}).sort('created');
    }
    query.exec((err, result)=>{
        if(err){
            res.status(500).send({message:err});
        }else{
            res.status(200).send(result);
        }
    });

}

//eliminar medicos

function deleteMedical(req, res){

    let id =req.params.id;

    Medical.findByIdAndDelete (id, (err, result) => {
        if(err){
            res.status(500).send({message:err});
        }else{
            res.status(200).send({message: result});
        }
        
    });
}

//Actualizar medicos

function updateMedical(req, res){
    let id=req.params.id
    let data = req.body;

    Medical.findByIdAndUpdate(id, data, {  new: true},(err, result) => {
        if(err){
            res.status(500).send({message: err});
        }else{
            res.status(200).send({message: result});
        }
    });
}

module.exports = {saveMedical, listMedical, findMedical, especialidadMedical, deleteMedical, updateMedical};
