let Patient = require("../models/paciente");


// función Guardar Pacientes

function savePatient(req, res){
    
    let myPatient = new Patient(req.body);

    myPatient.save((err, result)=>{
        if(err){
            res.status(500).send({message: err});
        }else{
            res.status(200).send({message:result});
        }
    });
}

// CONSULTA DE pacientes

function listPatient (req, res){
    let query = Patient.find({}).sort('content');
    query.exec((err, result)=>{
        if(err){
            res.status(500).send({message:err});
        }else{
            res.status(200).send(result);
        }
    });

}

function findPatient(req, res){

    let id = req.params.id;
    let query = Patient.findById (id);

    query.exec((err, result)=>{
        if(err){
            res.status(500).send({message: err});
        }else{
            res.status(200).send({message: result});
        }
    });
}

//eliminar medicos

function deletePatient(req, res){

    let id =req.params.id;

    Patient.findByIdAndDelete (id, (err, result) => {
        if(err){
            res.status(500).send({message:err});
        }else{
            res.status(200).send({message: result});
        }
        
    });
}

//Actualizar Pacientes

function updatePatient(req, res){
    let id=req.params.id
    let data = req.body;

    Patient.findByIdAndUpdate(id, data, {  new: true},(err, result) => {
        if(err){
            res.status(500).send({message: err});
        }else{
            res.status(200).send({message: result});
        }
    });
}
module.exports = {savePatient,listPatient,findPatient, deletePatient, updatePatient};
