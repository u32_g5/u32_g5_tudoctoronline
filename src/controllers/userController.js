let User = require('../models/user');

function saveUser( req, res ){

    let myUser = new User( req.body );

    myUser.save( ( err, result ) => {
        if(err){
            res.status(500).send( { message: err } );
        }else{
            res.status(200).send( { message: result });
        }
    });
}

module.exports = { saveUser }