const {Router} = require('express');
const medicalController = require('../controllers/medicalController');
const loginController = require('../controllers/loginController');
const userController = require('../controllers/userController');

//pacientes
const pacienteController = require('../controllers/pacienteController');

let router = Router ();

// Crud Medical
router.post('/medical/save', medicalController.saveMedical);
router.get('/medical/list',medicalController.listMedical);
router.get('/medical/:id', medicalController.findMedical);
//consultar por especialidad
router.get('/medical/list/:search',medicalController.especialidadMedical);
router.delete('/medical/:id', medicalController.deleteMedical);
router.put ('/medical/actualizar/:id', medicalController.updateMedical);


//Login
router.post('/login/verification', loginController.login );
router.post('/login/test',loginController.verifyToken, loginController.test );

//user

router.post('/user/save',userController.saveUser);


//modulo de pacientes

// Guardar pacientes
router.post('/patient/save', pacienteController.savePatient);
router.get('/patient/list',pacienteController.listPatient);
router.get('/patient/:id', pacienteController.findPatient);
// crear una ruta que busque los pacientes de acuerdo al medico que haya seleccionadp

router.delete('/patient/:id', pacienteController.deletePatient);
router.put ('/patient/actualizar/:id', pacienteController.updatePatient);


module.exports = router;
