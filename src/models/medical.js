const mongoose = require('mongoose');

let Schema = mongoose.Schema;

const medicalSchema = new Schema ({
    nameMedical: {type: String, required: true},
    code: {type: String, required: true},
    specialty: {type: String, required: true}

});

const Medical =mongoose.model('medicals',medicalSchema);

module.exports = Medical;
