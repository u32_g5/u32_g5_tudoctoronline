const mongoose = require('mongoose');

let Schema = mongoose.Schema;

const patientSchema = new Schema ({
    nombrePaciente: {type: String, required: true},
    docuemento:{type: String, required: true},
    correo: {type: String, required: true},
    telefono:{type: String, required: true},
    direccion: {type: String, required: true},
    genero: {type: String, required: true},
    fecha: {type: String, required: true},
});

const Patient =mongoose.model('patients',patientSchema);

module.exports = Patient;
