const mongoose = require('mongoose');

let Schema = mongoose.Schema;

const citaSchema = new Schema ({
    dateCita: Date,
    // hour: , Definir que tipo es
    lugar: {type: String, required: true},
    created: {type: Date, default: Date.now}

});

const CitaMedical =mongoose.model('citaMedicals',citaSchema);

module.exports = CitaMedical;
